a = "My name is Amandeep Kaur"
a = 10
a = 10.5
print(a)
print(type(a))

#Python Data Types
# str
# int
# float
# boolean
# list
# tuple
# dictionary
# set

b = 1+4j
print(type(b))

c = []
d = ()
e = {'Name':'Amandeep Kaur'}
f = {0}
g = set()
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))